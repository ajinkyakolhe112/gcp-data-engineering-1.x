## Artificial Intelligence Overview
- Different Usecases
    - https://appliedai.com/use-cases/1

## Machine Learning on GCP
1. Taxi Price Prediction in BigQueryML, https://cloud.google.com/blog/products/ai-machine-learning/bigquery-ml-and-bigquery-gis-used-together-predict-nyc-taxi-trip-cost & https://github.com/GoogleCloudPlatform/training-data-analyst/blob/master/blogs/bqml/taxifare_bqml.ipynb


# Extra
## Path to Learn Machine Learning
### Machine Learning with scikit-learn
- Basic
    - kaggle.com/learn
    - https://www.dataschool.io/machine-learning-with-scikit-learn/
    - https://www.udemy.com/python-for-data-science-and-machine-learning-bootcamp
- Intermediate
    - https://www.coursera.org/specializations/machine-learning
    - https://www.coursera.org/specializations/data-science-python
    - Book: Hands On Machine Learning with Scikit Learn and TensorFlow O'Reilly
    - http://course.fast.ai/ml.html

### Machine Learning & Deep Learning using Tensorflow
- https://www.udemy.com/zero-to-deep-learning/
- https://www.udemy.com/complete-guide-to-tensorflow-for-deep-learning-with-python
- Book: TensorFlow for Deep Learning O’Reilly
- http://course18.fast.ai/index.html
- https://developers.google.com/machine-learning/crash-course/
- Coursera: Machine Learning with TensorFlow on Google Cloud Platform Specialization
