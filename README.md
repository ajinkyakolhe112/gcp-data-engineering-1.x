# First login Instructions

1. Go to www.googlecloud.qwiklabs.com & create account with company email
  1. If it says, username is already taken, say forgot password, and retrieve the password and then login. 
1. If you can't find Data Engineering Lab in Classroom Options, ask instructor to add you to the classroom

# Starting Lab
1. **After Loging in to qwiklabs using company email id** & navigate to corresponding lab & click start lab. It will start the timer & user-name & password will be generated
1. **Open Incognito window** & go to www.console.cloud.google.com & login using **tmp username & password created by qwiklabs**
1. Change the project by clicking on "select project" and choose the project which begins with "qwiklabs-gcp-<something>"
1. **Select the project first & then launch the cloud shell** otherwise you will get an error